#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/5/2021
#
#   File   : simulate_bep.py
#
#----------------------------------------------------------------------
import sys
sys.path.append('c:\\jstraughan\\dev\\sw\\space-comm\\simulation')

from BEP.BEP import BEP
from Utils.plot_utils import plot_bep


if __name__ == "__main__":
    MIN_EB_NO = 0
    MAX_EB_NO = 16
    eb_no_vals = list(range(MIN_EB_NO, MAX_EB_NO + 1))

    _bpsk_bep = BEP(2, "PSK", eb_no_vals)
    _qpsk_bep = BEP(4, "PSK", eb_no_vals)
    _8psk_bep = BEP(8, "PSK", eb_no_vals)
    _16qam_bep = BEP(16, "QAM", eb_no_vals)
    _32qam_bep = BEP(32, "QAM", eb_no_vals)
    _64qam_bep = BEP(64, "QAM", eb_no_vals)
    _128qam_bep = BEP(128, "QAM", eb_no_vals)
    _256qam_bep = BEP(256, "QAM", eb_no_vals)

    plot_bep([_bpsk_bep, _qpsk_bep, _8psk_bep, _16qam_bep, _32qam_bep,
              _64qam_bep, _128qam_bep, _256qam_bep])
