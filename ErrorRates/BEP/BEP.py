#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/5/2021
#
#   File   : bep.py
#
#----------------------------------------------------------------------
from Utils.bep_utils import calculate_mpsk_bep, calculate_mqam_bep


class BEP(object):
    def __init__(self, mod_order, scheme, eb_no_vals):
        self.mod_order = mod_order
        self.scheme = scheme
        self.eb_no_vals = eb_no_vals
        self.bep = []
        if mod_order == 2 and scheme == "PSK":
            self.mod_name = "BPSK"
        elif mod_order == 4 and scheme == "PSK":
            self.mod_name = "QPSK"
        else:
            self.mod_name = f"{mod_order}-{scheme}"
        self.main()

    def main(self):
        self.calculate_bep()

    def calculate_bep(self):
        if self.scheme == "PSK":
            self.bep = calculate_mpsk_bep(self.mod_order, self.eb_no_vals)
        elif self.scheme == "QAM":
            self.bep = calculate_mqam_bep(self.mod_order, self.eb_no_vals)
