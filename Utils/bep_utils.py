#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/5/2021
#
#   File   : bep_utils.py
#
#----------------------------------------------------------------------
import math


def q_function_approx(value):
    """
    Approximates Q(value) using error function
    """
    return 0.5 - 0.5 * math.erf(value / math.sqrt(2))


def convert_db_to_linear(values):
    """
    Converts a list of values in dB to linear scale
    """
    return [math.pow(10, v/10) for v in values]


def calculate_mpsk_bep(mod_order, eb_no_vals):
    """
    Calculate the bit error probability of an m-psk modulation scheme
    """
    bep = []
    eb_no_vals = convert_db_to_linear(eb_no_vals)
    for eb_no in eb_no_vals:
        sum = 0
        coefficient = 2 / max(math.log2(mod_order), 2)
        for idx in range(1, int(max(mod_order/4, 1) + 1)):
            val = math.sqrt(2*eb_no*math.log2(mod_order)) * math.sin((2*idx-1)*math.pi/mod_order)
            sum += q_function_approx(val)
        bep.append(coefficient * sum)
    return bep


def calculate_mqam_bep(mod_order, eb_no_vals):
    """
    Calculate the bit error probability of an m-qam modulation scheme
    """
    bep = []
    eb_no_vals = convert_db_to_linear(eb_no_vals)
    for eb_no in eb_no_vals:
        sum = 0
        coefficient = (4/math.log2(mod_order)) * (1-1/math.sqrt(mod_order))
        for idx in range(1, math.ceil(math.sqrt(mod_order)/2) + 1):
            val = (2*idx-1) * math.sqrt((3*eb_no*math.log2(mod_order)) / (mod_order - 1))
            sum += q_function_approx(val)
        bep.append(coefficient * sum)
    return bep
