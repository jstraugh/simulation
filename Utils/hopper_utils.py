#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/12/2021
#
#   File   : hopper_utils.py
#
#----------------------------------------------------------------------


def generate_prn7(num_bits):
    """
    Generate a simple maximal length PRN7 binary sequence
    """
    shift_reg = [1, 1, 1, 1, 1]
    sequence = []
    for _ in range(num_bits):
        lsb = shift_reg[-1]
        msb = lsb ^ shift_reg[-3]
        shift_reg.pop(-1)
        shift_reg.insert(0, msb)
        sequence.append(lsb)
    return sequence


def map_sequence(sequence, signal_power):
    """
    Get the frequency mapping from a sequence of bits
    """
    idx = 0
    maps = []
    for _ in range(int(len(sequence) / 3)):
        gray_code = f"{sequence[idx]}{sequence[idx+1]}{sequence[idx+2]}"
        freq = gray_code_to_frequency_map(gray_code)
        hop = map_frequency_to_hop(freq, signal_power)
        maps.append(hop)
        idx += 3
    return maps


def gray_code_to_frequency_map(code):
    """
    Map a 3-bit Gray code sequence to a center frequency
    """
    freq = float()
    if code == '000':
        freq =  100
    elif code == '001':
        freq = 110
    elif code == '011':
        freq = 120
    elif code == '010':
        freq = 130
    elif code == '110':
        freq = 140
    elif code == '111':
        freq = 150
    elif code == '101':
        freq = 160
    elif code == '100':
        freq = 170
    return freq


def map_frequency_to_hop(freq, signal_power):
    """
    There are 8 frequencies being used in this simulation
    (2^3). Set the signal power at the desired index
    """
    hop = []
    if freq == 100:
        hop = [signal_power, 0, 0, 0, 0, 0, 0, 0]
    elif freq == 110:
        hop = [0, signal_power, 0, 0, 0, 0, 0, 0]
    elif freq == 120:
        hop = [0, 0, signal_power, 0, 0, 0, 0, 0]
    elif freq == 130:
        hop = [0, 0, 0, signal_power, 0, 0, 0, 0]
    elif freq == 140:
        hop = [0, 0, 0, 0, signal_power, 0, 0, 0]
    elif freq == 150:
        hop = [0, 0, 0, 0, 0, signal_power, 0, 0]
    elif freq == 160:
        hop = [0, 0, 0, 0, 0, 0, signal_power, 0]
    elif freq == 170:
        hop = [0, 0, 0, 0, 0, 0, 0, signal_power]
    else:
        raise ValueError('Invalid frequency mapping')
    return hop


def estimate_snr(signal_spectrum, jamming_spectrum):
    """
    Based on noise present, approximate the SNR
    """
    snrs = []
    for hop in signal_spectrum:
        idx = int([i for i, e in enumerate(hop) if e != 0][0])
        snr = hop[idx] / jamming_spectrum[idx]
        snrs.append(snr)
    return snrs


def get_modulation_map(snr_map):
    """
    Based on results, the optimal mapping to keep BEP at least
    less than 0.01
    """
    modulations = []
    for snr in snr_map:
        modulation = ''
        if snr >= 15:
            modulation = '128-QAM'
        elif snr >= 13:
            modulation = '64-QAM'
        elif snr >= 10:
            modulation = '16-QAM'
        elif snr >= 8:
            modulation = '8-PSK'
        elif snr >= 4:
            modulation = 'QPSK'
        else:
            modulation = 'BPSK'
        modulations.append(modulation)
    return modulations
