#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/12/2021
#
#   File   : plot_utils.py
#
#----------------------------------------------------------------------
import numpy as np
from matplotlib import pyplot as plt


def plot_bep(mods):
    """
    Creates a plot of the bit error rate for each modulation given
    """
    plt.title(f"Bit Error Probability")
    plt.xlabel("Eb/No (dB)")
    plt.ylabel("BEP")
    plt.yscale("log")
    plt.grid()
    markers = ['h', 'D', '.', 'o', 'v', '>', 's', '+']
    for idx, mod in enumerate(mods):
        plt.plot(mod.eb_no_vals, mod.bep, label=f"{mod.mod_name}",
                 marker=markers[idx])
    plt.legend(loc='lower left')
    plt.show()


def plot_hopping(hops, noise, jam, mods, snrs, delay):
    """
    Plot the FHSS with 'delay' seconds between each hop
    """
    for idx, _ in enumerate(hops):
        plot_frame(hops[idx], mods[idx], snrs[idx], noise, jam)
        plt.pause(delay)
        plt.cla()
        plt.draw()
    plt.close()


def plot_frame(hop, mod, snr, noise, jam):
    """
    Plot the signal, noise, and jam for each hop
    """
    all_frequencies = [100, 110, 120, 130,
                       140, 150, 160, 170]

    plt.bar(all_frequencies, hop, color='g', label='Signal')
    plt.bar(all_frequencies, jam, color='r', label='Jammer')
    plt.bar(all_frequencies, noise, color='k', label='Noise')
    plt.legend(loc='upper right')
    plt.xticks(np.arange(100, 180, 10))
    plt.xlabel("Center Frequency (MHz)")
    plt.ylabel("Signal Power (dB)")
    plt.title("Frequency Hopping Simulation")
    plt.annotate('Mod   : {}'.format(mod), xy=(98, 14.5))
    plt.annotate('Eb_No: {:>2.2f}'.format(snr), xy=(98, 13.8))
