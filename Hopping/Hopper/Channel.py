#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/12/2021
#
#   File   : Channel.py
#
#----------------------------------------------------------------------

class Channel(object):
    def __init__(self, noise_power, jammer=None, awgn=True):
        self.noise_power = noise_power
        self.jammer = jammer
        self.awgn = awgn
        self.spectrum = []
        self.main()

    def main(self):
        """
        High level model of a channel. Out of the 8 frequencies used
        in this simulation, set each index based on if the channel
        is assumed AWGN or not. If not, the noise power varies
        """
        if self.awgn:
            self.spectrum = [self.noise_power for _ in range(8)]
        else:
            self.spectrum = [1.05, 4.1, 2, 1.02, 1, 2.2, 3.2, 1.07]

    def get_noise_only_spectrum(self):
        return self.spectrum

    def get_jammed_only_spectrum(self):
        return self.jammer.spectrum

    def get_jammed_spectrum(self):
        jam_spec = self.jammer.spectrum
        noise_spec = self.spectrum
        return [noise_spec[i] + jam_spec[i] for i in range(8)]
