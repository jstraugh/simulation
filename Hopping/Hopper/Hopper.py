#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/12/2021
#
#   File   : Hopper.py
#
#----------------------------------------------------------------------
from Utils import hopper_utils, plot_utils

class Hopper(object):
    def __init__(self, num_hops, signal_power, channel):
        self.num_hops = num_hops
        self.signal_power = signal_power
        self.channel = channel
        self.prn_sequence = []
        self.modulation_map = []
        self.signal_spectrum = []
        self.noise_spectrum = []
        self.jam_spectrum  = []
        self.all_interference = []
        self.main()

    def main(self):
        """
        Very high level model of an adaptive modulation FHSS system
        """
        self.prn_sequence = hopper_utils.generate_prn7(num_bits=3*self.num_hops)
        self.signal_spectrum = hopper_utils.map_sequence(self.prn_sequence, self.signal_power)
        self.noise_spectrum = self.channel.get_noise_only_spectrum()
        self.jam_spectrum = self.channel.get_jammed_only_spectrum()
        self.all_interference = self.channel.get_jammed_spectrum()

        self.snr_map = hopper_utils.estimate_snr(self.signal_spectrum, self.all_interference)
        self.modulation_map = hopper_utils.get_modulation_map(self.snr_map)

        plot_utils.plot_hopping(self.signal_spectrum, self.noise_spectrum, self.jam_spectrum,
                                self.modulation_map, self.snr_map, 0.2)