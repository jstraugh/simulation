#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/14/2021
#
#   File   : Jammer.py
#
#----------------------------------------------------------------------


class Jammer(object):
    def __init__(self, jamming_power, jamming_freq):
        self.jamming_power = jamming_power
        self.jamming_freq = jamming_freq
        self.spectrum = []
        self.jam()

    def jam(self):
        """
        Out of the 8 frequencies used in this simulation, set the jamming
        power of the desired frequency
        """
        if self.jamming_freq == 100:
            self.spectrum = [self.jamming_power, 0, 0, 0, 0, 0, 0, 0]
        elif self.jamming_freq == 110:
            self.spectrum = [0, self.jamming_power, 0, 0, 0, 0, 0, 0]
        elif self.jamming_freq == 120:
            self.spectrum = [0, 0, self.jamming_power, 0, 0, 0, 0, 0]
        elif self.jamming_freq == 130:
            self.spectrum = [0, 0, 0, self.jamming_power, 0, 0, 0, 0]
        elif self.jamming_freq == 140:
            self.spectrum = [0, 0, 0, 0, self.jamming_power, 0, 0, 0]
        elif self.jamming_freq == 150:
            self.spectrum = [0, 0, 0, 0, 0, self.jamming_power, 0, 0]
        elif self.jamming_freq == 160:
            self.spectrum = [0, 0, 0, 0, 0, 0, self.jamming_power, 0]
        elif self.jamming_freq == 170:
            self.spectrum = [0, 0, 0, 0, 0, 0, 0, self.jamming_power]
        else:
            raise ValueError('Invalid frequency mapping')
