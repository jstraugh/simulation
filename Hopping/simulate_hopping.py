#----------------------------------------------------------------------
#
#   Author : Jake Straughan
#   Course : UCCS ECE 5965 - Space Communications
#   Date   : 12/12/2021
#
#   File   : simulate_hopping.py
#
#----------------------------------------------------------------------
import sys
import time
sys.path.append('c:\\jstraughan\\dev\\sw\\space-comm\\simulation')

from Hopper.Hopper import Hopper
from Hopper.Channel import Channel
from Hopper.Jammer import Jammer

if __name__ == "__main__":
    NUM_HOPS = 50
    JAM_POWER = 4
    NOISE_POWER = 1
    SIGNAL_POWER = 15

    # No jamming
    narrowband_jammer = Jammer(0, 130)
    awgn_channel = Channel(NOISE_POWER, narrowband_jammer, awgn=True)
    fhss_system = Hopper(NUM_HOPS, SIGNAL_POWER, awgn_channel)
    time.sleep(3)

    # Narrowband jamming
    narrowband_jammer = Jammer(JAM_POWER, 130)
    awgn_channel = Channel(NOISE_POWER, narrowband_jammer, awgn=True)
    fhss_system = Hopper(NUM_HOPS, SIGNAL_POWER, awgn_channel)
    time.sleep(3)

    # Noise and narrowband jamming
    narrowband_jammer = Jammer(JAM_POWER*2, 160)
    random_channel = Channel(NOISE_POWER, narrowband_jammer, awgn=False)
    fhss_system = Hopper(NUM_HOPS, SIGNAL_POWER, random_channel)